package com.atguigu.gulimail.order.vo;

import com.atguigu.gulimail.order.entity.OrderEntity;
import lombok.Data;

import java.util.PriorityQueue;

/**
 * @author pxie2120
 * @create 2023/5/11
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code;//0成功，其他都是错误码
}
