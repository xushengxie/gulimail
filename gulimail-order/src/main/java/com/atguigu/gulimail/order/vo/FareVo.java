package com.atguigu.gulimail.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author pxie2120
 * @create 2023/5/12
 */
@Data
public class FareVo {
    private MemeberAddressVo address;
    private BigDecimal fare;
}
