package com.atguigu.gulimail.order.config;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.annotation.PostConstruct;

/**
 * @author pxie2120
 * @create 2023/4/26
 */
@Configuration
public class MyRabbitConfig {
//    @Autowired
    RabbitTemplate rabbitTemplate;

/*    public MyRabbitConfig(RabbitTemplate rabbitTemplate){
        this.rabbitTemplate = rabbitTemplate;
        initRabbitTemplate();
    }*/
    //TODO
    @Primary
    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory){
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        this.rabbitTemplate = rabbitTemplate;
        rabbitTemplate.setMessageConverter(messageConverter());
        initRabbitTemplate();
        return rabbitTemplate;
    }

    /**
     *使用JSON序列化机制，进行消息转换
     * @return
     */
    @Bean
    public MessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }

    /**
     * 定制RabbitTemplate
     */
//    @PostConstruct//MyRabbitConfig对象创建完成之后执行这个方法
    public void initRabbitTemplate(){
        //设置发送端到达Broker确认的回调
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            /**
             *
             * @param correlationData 当前消息的唯一关联数据（这个是消息的唯一Id）
             * @param ack 消息是否成功收到 <---- 只要消息抵达 Broker 就 ack = true
             * @param cause 失败的原因
             */
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
                /**
                 * 1、做好消息确认（publisher，consumer【手动ACk】）
                 * 2、每一个发送的消息在数据库中做好记录，定期的失败的消息再次发送
                 */
                //服务器收到了
                System.out.println("confirm...correlationData["+correlationData+"]===>ack["+ack+"]===>cause["+cause+"]");
            }
        });

        //设置从Broker到Queue确认的回调
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            /**
             * 只要有某个消息没有投递到指定的队列，就触发这个回调
             * @param message 投递失败的消息的详细信息
             * @param i 回复的状态码
             * @param s 回复的文本内容
             * @param s1  当时这个消息返送给哪个交换机
             * @param s2 当时这个消息返送给哪个路由键
             */
            @Override
            public void returnedMessage(Message message, int i, String s, String s1, String s2) {
                //报错了，就需要修改数据库中当前消息的状态
                System.out.println("失败的消息内容" + message);
            }
        });
    }
}
