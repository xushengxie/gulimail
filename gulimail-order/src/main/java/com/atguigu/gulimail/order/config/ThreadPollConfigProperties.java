package com.atguigu.gulimail.order.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author pxie2120
 * @create 2023/4/13
 */
@ConfigurationProperties(prefix = "gulimail.thread")
@Component
@Data
public class ThreadPollConfigProperties {
    private Integer coreSize;
    private Integer maxSize;
    private Integer keepAliveTime;
}
