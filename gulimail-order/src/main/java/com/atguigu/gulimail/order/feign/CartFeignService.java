package com.atguigu.gulimail.order.feign;

import com.atguigu.gulimail.order.vo.OrderItemVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/5/8
 */
@FeignClient("gulimail-cart")
public interface CartFeignService {

    @GetMapping("/currentUserCartItem")
    public List<OrderItemVo> getCurrentUserCartItem();
}
