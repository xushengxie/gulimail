package com.atguigu.gulimail.order.feign;

import com.atguigu.gulimail.order.vo.MemeberAddressVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author pxie2120
 * @create 2023/5/8
 */
@FeignClient("gulimail-member")
public interface MemberFeignService {
    @GetMapping("member/memberreceiveaddress/{memberId}/addresses")
    List<MemeberAddressVo> getAddress(@PathVariable("memberId") Long memberId);


}
