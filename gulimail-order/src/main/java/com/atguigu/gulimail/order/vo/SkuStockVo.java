package com.atguigu.gulimail.order.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/5/10
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
