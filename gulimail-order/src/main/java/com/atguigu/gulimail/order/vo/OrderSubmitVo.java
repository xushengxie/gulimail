package com.atguigu.gulimail.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author pxie2120
 * @create 2023/5/11
 * 封装订单提交的数据
 */
@Data
public class OrderSubmitVo {
    private Long addrId;//收货地址的Id
    private Integer payType; //支付方式
    //无需提交需要购买的商品，无购物车再获取一遍
    //优惠、发票
    private String note;//订单备注
    private String  orderToken;//防重令牌
    private BigDecimal payPrice;//应付价格，进行验价

    //用户相关信息可以直接去session取
}
