package com.atguigu.gulimail.order.constant;

/**
 * @author pxie2120
 * @create 2023/5/11
 */
public class OrderConstant {
    public static final String USER_ORDER__TOKEN_PREFIX = "orer:token:";
}
