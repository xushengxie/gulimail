package com.atguigu.gulimail.order.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @author pxie2120
 * @create 2023/5/8
 * 订单确认页需要用的数据
 */

public class OrderConfirmVo {
    //收货地址
    @Setter @Getter
    List<MemeberAddressVo> address;

    //展示的订单数据为选中的购物项
    @Setter @Getter
    List<OrderItemVo> items;

    //发票.....

    //优惠券信息
    @Setter @Getter
    Integer integration;

    ///库存信息
    @Setter @Getter
    Map<Long, Boolean> stocks;

    //防重令牌
    @Setter @Getter
    String orderToken;
    public Integer getCount(){
        Integer i = 0;
        for (OrderItemVo item : items) {
            i += item.getCount();
        }
        return i;
    }
    //计算订单总额
    public BigDecimal getTotal() {
        BigDecimal total = new BigDecimal("0");
        if(items != null){
            for (OrderItemVo item : items) {
                BigDecimal multiply = item.getPrice().multiply(new BigDecimal(item.getCount().toString()));
                total = total.add(multiply);
            }
        }
        return total;
    }

    //实际应付总额
    public BigDecimal getPayPrice() {
        return getTotal();
    }
}
