package com.atguigu.gulimail.order.web;

import com.atguigu.common.exception.NoStockException;
import com.atguigu.gulimail.order.service.OrderService;
import com.atguigu.gulimail.order.vo.OrderConfirmVo;
import com.atguigu.gulimail.order.vo.OrderSubmitVo;
import com.atguigu.gulimail.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutionException;

/**
 * @author pxie2120
 * @create 2023/5/8
 */
@Controller
public class OrderWebController {
    @Autowired
    OrderService orderService;

    @GetMapping("toTrade")
    public String toTrade(Model model, HttpServletRequest request) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData", confirmVo);
        return "confirm";
    }

    /**
     * 下单功能
     * @param vo
     * @return
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes){
        try{
            SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);
            //下单：创建订单----》验令牌----》验价格----》锁库存
            //下单成功跳转到支付页面
            //下单失败回到订单确认页重新确认订单信息
            System.out.println("订单提交的数据"+vo);
            if(responseVo.getCode() == 0){
                //下单成功来到支付选择页
                model.addAttribute("submitOrderResp", responseVo);
                return "pay";
            }else {
                //下单失败
                String msg = "下单失败";
                switch (responseVo.getCode()){
                    case 1: msg += "订单信息过期，请刷新后重新提交"; break;
                    case 2: msg += "订单商品价格发生变化，请确认后再次提交"; break;
                    case 3: msg += "库存锁定失败，商品库存不足"; break;
                }
                redirectAttributes.addAttribute("msg", msg);
                System.out.println(msg);
                return "redirect:http://order.pxie2120.com/toTrade";
            }
        }catch (Exception e){
            if(e instanceof NoStockException){
//                String msg = ((NoStockException)e).getMessage();
//                redirectAttributes.addAttribute("msg", msg);
                System.out.println("库存不足");
            }
            return "redirect:http://order.pxie2120.com/toTrade";
        }
    }
}
