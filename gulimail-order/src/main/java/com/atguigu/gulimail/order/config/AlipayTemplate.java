package com.atguigu.gulimail.order.config;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.atguigu.gulimail.order.vo.PayVo;
import lombok.Data;
import org.springframework.stereotype.Component;

//@ConfigurationProperties(prefix = "alipay")
@Component
@Data
public class AlipayTemplate {

    //在支付宝创建的应用的id
    private   String app_id = "2021000122693577";

    // 商户私钥，您的PKCS8格式RSA2私钥
    private  String merchant_private_key = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCagOQZOuDTX3C3taR+bc+EVyg3hgeon/GC+bBGbyovDVJFn8Gd4SY07tuNf0h5FF23dn6TOPW2LtqpW/57pyeRGDJS7vMA31RMngO7FwVm+iveH+ZjoPueMbek2bP3P/ngEANmVijqyqbjmmORURResR7rqHjGv+YVqzoRgMPNz5xmCWMKRUkIFMlUR5HB9cOCiVBWDpXvhONKVUQhpZaP8QjfUGckeQY2SCgdb+0HHVvW9tjA3+cd2cg4mc1MXaOC1evp3j4WqoELMpnAkOUzIiikXUaOSVGgw+Fx/jtaMsDWlTpeyUVHmsGZXKvsOLwvDw+5/dN5zppgpCeXMZZzAgMBAAECggEAD5hI7b8CqHMutmxQMS62Tn5ZodkWds/GGdlIEKPx1xP6JOruKRhrudYONScO0qHan8UHG6cZLg1mpseRVTvXA480rShPQNxJ7v3YFI9LTUydFuuTXqUGIL6V8b7DqoY7m4EiRrcVpDVWSQzxVd6gNrqcC19S6YS/HZrN3qxnlyNgJ1rqpTncFSLV3l6VXMtrBwHmOH+Q19Mt8dNhKkamU2EqlfPaIKIgU4A05YUmYYLEtmacH5gui3Jen6gOm5ubbCnHGJWpeZ1gXHXSqCePi/ARv8HXJIEJTDpgCIpkmAEmSW16b8IKfETLtxO8QL5MNXU3X5K//p8n/c+ZFS70cQKBgQDbZT1BNn64JqGV+SPN1ky4jZfcrG/ZZea4kBhKDfPN+w0uc1tr61kaFZ/Al9i6wuFRVOZ4Q/uaka4uRcnYfkmrNc3S34gG4yg5RHC8UktcZf1RDjAH/cOMWljVNNAozlht4RJ5hxBHXIEAhCeBc7loFq0it5MJzQ1a1Kpy/nq9OwKBgQC0SACl3anMyc65yqfH+Cs8eetAQPHtmA1SbvRMe36tJGi2Hdf0C3zXq3QuzF74HRpm9KRYWlwR5JBzXRg8zMD/AT/auC+rDt2m3lGDuaaREFeDMFWcvzAO1VmBsk7igRqrtt1a5d5KB3Tuj2eUQfyQNKfc9QWufjtSoIp8aadYKQKBgA0i3qwM60FzbrFgHXixoB5cu/8GhBvsMghS+GFWvIZUxdFGLu8jRE5/hPUvIlbsXDlWcAY7a9Exb9BtdUx1I0wRCKXbpHVlHH9xTZ64/rIlmSlFRDL2SsMYlcEOTHhlAn5xv31vTNY4/7BcOVFS8IL++JbfjvpDQM1zdlE0U30JAoGBALPc9lCFg4gee5Jfft+CWFig5rWXVgeO4ROyQSghObQylAnGYpNH9skRdPvPyJxQJOw8/c5scFklqXx/j9PxEEgajgOAKYbHcRqvybHbQjSOklD/oxqcoKzb7M36ALv4xIw3CEdM+WbwY5gzGMsQvpN9BVDsYDvEExmPtDsGCgBpAoGBAKnSsmv2cp7urn2ec8LnqIPWaIJzAZbU7+R+DQ/SaFTGCGbp2gPl+OdTzgIyTnWZgDmUULgcMgVJM8v4o18jOH46DrdOgJB/8rrTwKa+w+qg9cWSpXvgZLeP2KPABBNn1ayCezsWySsfwwyZgtJX+oZ9O2SssXGq1T3TDOH1Rsm2";
    // 支付宝公钥,查看地址：https://openhome.alipay.com/platform/keyManage.htm 对应APPID下的支付宝公钥。
    private  String alipay_public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg9hk0qYCGTT2zCkzQgXwP2TIg84dcB7xY1m3Rowrm0ZYWw8B0tEpNLsYWbsIo2aNFY4OM5kFx8A8SrBr9KCThUTqIHLMFtvhDbkBbUD3gIeTrQrbicfP1tKvsFvZNv83VZCscmQrh5zDYZEN34dEOHnKwSFI3eWK/BkXxO2acm1Trd8f1RuVI5IpYHY3UNHlufjlcyXE9+dd91Uj/XFGjHA0O99FchqahldZ6eORkugfGuqLd50KNqdqp7zSksjzngapV9WIF/aU+VrDBMoG2C4jj1HH8UuoxnrqMbuDVQA88bGI+zfAfMWnY2jgQgPQjFI/2TVHx8Zf/1ISrE4KnQIDAQAB";
    // 服务器[异步通知]页面路径  需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    // 支付宝会悄悄的给我们发送一个请求，告诉我们支付成功的信息
    private  String notify_url = "http://haf67x.natappfree.cc/payed/notify";

    // 页面跳转同步通知页面路径 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
    //同步通知，支付成功，一般跳转到成功页
    private  String return_url = " http://member.pxie2120.com/memberOrder.html";

    // 签名方式
    private  String sign_type = "RSA2";

    // 字符编码格式
    private  String charset = "utf-8";

    //支付超时时间
    private String timeout = "30m";

    // 支付宝网关； https://openapi.alipaydev.com/gateway.do
    private  String gatewayUrl = "https://openapi.alipaydev.com/gateway.do";

    public  String pay(PayVo vo) throws AlipayApiException {

        //AlipayClient alipayClient = new DefaultAlipayClient(AlipayTemplate.gatewayUrl, AlipayTemplate.app_id, AlipayTemplate.merchant_private_key, "json", AlipayTemplate.charset, AlipayTemplate.alipay_public_key, AlipayTemplate.sign_type);
        //1、根据支付宝的配置生成一个支付客户端
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                app_id, merchant_private_key, "json",
                charset, alipay_public_key, sign_type);

        //2、创建一个支付请求 //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(return_url);
        alipayRequest.setNotifyUrl(notify_url);

        //商户订单号，商户网站订单系统中唯一订单号，必填
        String out_trade_no = vo.getOut_trade_no();
        //付款金额，必填
        String total_amount = vo.getTotal_amount();
        //订单名称，必填
        String subject = vo.getSubject();
        //商品描述，可空
        String body = vo.getBody();

        alipayRequest.setBizContent("{\"out_trade_no\":\""+ out_trade_no +"\","
                + "\"total_amount\":\""+ total_amount +"\","
                + "\"subject\":\""+ subject +"\","
                + "\"body\":\""+ body +"\","
                + "\"timeout_express\":\""+timeout+"\","
                + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

        String result = alipayClient.pageExecute(alipayRequest).getBody();

        //会收到支付宝的响应，响应的是一个页面，只要浏览器显示这个页面，就会自动来到支付宝的收银台页面
        System.out.println("支付宝的响应："+result);

        return result;

    }
}
