package com.atguigu.gulimail.order;

import com.atguigu.gulimail.order.entity.OrderEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.rmi.server.UID;
import java.util.UUID;

@Slf4j
@SpringBootTest
class GulimailOrderApplicationTests {

    @Autowired
    AmqpAdmin amqpAdmin; //负责创建队列，交换机以及绑定关系和设定路由键

    @Autowired
    RabbitTemplate rabbitTemplate; //收发消息

    @Test
    void sendMessage(){
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderSn("libia");
        orderEntity.setCouponId(1L);
        rabbitTemplate.convertAndSend("hello-java-exchange", "hello-java", orderEntity, new CorrelationData(UUID.randomUUID().toString()));
    }

    @Test
    void createExchange() {
        //public DirectExchange(String name, boolean durable, boolean autoDelete, Map<String, Object> arguments)
        DirectExchange directExchange = new DirectExchange("hello-java-exchange", true, false);
        amqpAdmin.declareExchange(directExchange);
        log.info("交换机创建完成");
    }

    @Test
    void createQueue(){
        //public Queue(String name, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
        Queue queue = new Queue("hello-java-queue", true, false, false);
        amqpAdmin.declareQueue(queue);
        log.info("队列创建完成");
    }

    @Test
    void createBinding(){
        //public Binding(String destination, DestinationType destinationType, String exchange, String routingKey,Map<String, Object> arguments)
        Binding binding = new Binding("hello-java-queue", Binding.DestinationType.QUEUE, "hello-java-exchange",
                "hello-java", null
                );
        amqpAdmin.declareBinding(binding);
        log.info("Binding创建完成");
    }

}
