package com.atguigu.gulimail.member.config;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author pxie2120
 * @create 2023/5/9
 */
@Configuration
public class GuliFeignConfig {
    @Bean("requestInterceptor")
    public RequestInterceptor requestInterceptor(){
        return new RequestInterceptor() {
            @Override
            public void apply(RequestTemplate requestTemplate) {
                //1、RequestContextHolder拿到刚进来的这个请求
                ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
                if(attributes!= null){
                    HttpServletRequest request = attributes.getRequest();//老请求
                    if(request != null){
                        //同步请求头数据，主要是Cookie
                        String cookie = request.getHeader("Cookie");
                        //给新请求同步老请求的Cookie
                        requestTemplate.header("Cookie", cookie);
                    }
                }
            }
        };
    }
}
