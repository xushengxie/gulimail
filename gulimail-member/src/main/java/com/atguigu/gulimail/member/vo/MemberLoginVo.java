package com.atguigu.gulimail.member.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/4/19
 */
@Data
public class MemberLoginVo {
    private String loginacct;
    private String password;
}
