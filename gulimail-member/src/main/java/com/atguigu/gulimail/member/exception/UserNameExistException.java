package com.atguigu.gulimail.member.exception;

/**
 * @author pxie2120
 * @create 2023/4/18
 */
public class UserNameExistException extends RuntimeException{
    public UserNameExistException() {
        super("用户名已存在");
    }
}
