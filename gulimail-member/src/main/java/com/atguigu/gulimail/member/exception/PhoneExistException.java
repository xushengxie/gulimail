package com.atguigu.gulimail.member.exception;

/**
 * @author pxie2120
 * @create 2023/4/18
 */
public class PhoneExistException extends RuntimeException{
    public PhoneExistException() {
        super("手机号已存在");
    }
}
