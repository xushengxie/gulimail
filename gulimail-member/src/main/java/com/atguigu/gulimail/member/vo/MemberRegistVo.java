package com.atguigu.gulimail.member.vo;

import lombok.Data;


/**
 * @author pxie2120
 * @create 2023/4/18
 */
@Data
public class MemberRegistVo {
    private String userName;

    private String password;

    private String phone;
}
