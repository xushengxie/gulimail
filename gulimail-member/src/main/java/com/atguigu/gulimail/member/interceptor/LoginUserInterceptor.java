package com.atguigu.gulimail.member.interceptor;

import com.alibaba.fastjson.JSON;
import com.atguigu.common.constant.AuthServerConstant;
import com.atguigu.common.vo.MemberResponseVo;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author pxie2120
 * @create 2023/5/8
 */
@Component
public class LoginUserInterceptor implements HandlerInterceptor {
    public static ThreadLocal<MemberResponseVo> loginUser = new ThreadLocal<>();

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //对于/order/order/status/**这样的请求直接放行
        ///member/memberreceiveaddress/info/{id}
        String uri = request.getRequestURI();
        boolean match = new AntPathMatcher().match("/member/**", uri);
        if(match){
            return  true;
        }

        Object object=  request.getSession().getAttribute(AuthServerConstant.LOGIN_USER);
        String s = JSON.toJSONString(object);
        MemberResponseVo attribute = JSON.parseObject(s, MemberResponseVo.class);
        if(attribute!= null){
            loginUser.set(attribute);
            return true;
        }else {
            //没登录就去登录
            request.getSession().setAttribute("msg", "请先去登录");
            response.sendRedirect("http://auth.pxie2120.com/login.html");
            return  false;
        }
    }
}
