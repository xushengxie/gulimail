package com.atguigu.gulimail.ware.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/3/28
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
