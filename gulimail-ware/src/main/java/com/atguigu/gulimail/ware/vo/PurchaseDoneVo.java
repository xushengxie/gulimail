package com.atguigu.gulimail.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author pxie2120
 * @create 2023/3/27
 */
@Data
public class PurchaseDoneVo {
    @NotNull
    private Long id; //采购单Id

    private List<PurchaseItemDoneVo> items;

}
