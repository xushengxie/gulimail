package com.atguigu.gulimail.ware.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/3/27
 */
@Data
public class PurchaseItemDoneVo {
    //itemId:1,status:4,reason:""
    private Long itemId;
    private Integer status;
    private String reason;
}
