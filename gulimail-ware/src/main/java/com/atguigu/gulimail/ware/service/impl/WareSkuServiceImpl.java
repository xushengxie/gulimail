package com.atguigu.gulimail.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.exception.NoStockException;
import com.atguigu.common.to.mq.OrderTo;
import com.atguigu.common.to.mq.StockDetailTo;
import com.atguigu.common.to.mq.StockLockedTo;
import com.atguigu.common.utils.R;
import com.atguigu.gulimail.ware.entity.WareOrderTaskDetailEntity;
import com.atguigu.gulimail.ware.entity.WareOrderTaskEntity;
import com.atguigu.gulimail.ware.feign.OrderFeignService;
import com.atguigu.gulimail.ware.service.WareOrderTaskDetailService;
import com.atguigu.gulimail.ware.service.WareOrderTaskService;
import com.atguigu.gulimail.ware.vo.OrderItemVo;
import com.atguigu.gulimail.ware.vo.OrderVo;
import com.atguigu.gulimail.ware.vo.SkuHasStockVo;
import com.atguigu.gulimail.ware.feign.ProductFeignService;
import com.atguigu.gulimail.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;

import com.atguigu.gulimail.ware.dao.WareSkuDao;
import com.atguigu.gulimail.ware.entity.WareSkuEntity;
import com.atguigu.gulimail.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {
    @Autowired
    ProductFeignService productFeignService;

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    WareOrderTaskService wareOrderTaskService;

    @Autowired
    WareOrderTaskDetailService wareOrderTaskDetailService;

    @Autowired
    OrderFeignService orderFeignService;

    /**
     * 库存自动解锁
     * 下订单成功，库存锁定成功，但是接下来的业务调用失败，导致订单回滚，之前锁定的库存一段时间过后就要自动解锁
     *
     * 只要解锁库存的消息失败，一定要告诉服务器此次解锁库存失败
     */

    private void unLockStock(Long skuId, Long wareId, Integer num, Long taskDetailId){
        //库存解锁
        wareSkuDao.unLockStock(skuId, wareId, num);
        //更新库存工作单的状态
        WareOrderTaskDetailEntity detailEntity = new WareOrderTaskDetailEntity();
        detailEntity.setId(taskDetailId);
        detailEntity.setLockStatus(2);//变为已解锁
        wareOrderTaskDetailService.updateById(detailEntity);
    }

    /**
     *  sku_id 1
     *  ware_id 2
     * @param params
     * @return
     */
    @Autowired
    WareSkuDao wareSkuDao;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();

        String skuId = (String) params.get("skuId");
        if(!StringUtils.isEmpty(skuId)){
            queryWrapper.eq("sku_id", skuId);
        }

        String wareId = (String) params.get("wareId");
        if(!StringUtils.isEmpty(wareId)){
            queryWrapper.eq("ware_id", wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        //判断如果还没有这个库存记录新增
        List<WareSkuEntity> entities = wareSkuDao.selectList(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));
        if(entities.size() == 0 || entities == null){
            WareSkuEntity skuEntity = new WareSkuEntity();
            skuEntity.setSkuId(skuId);
            skuEntity.setStock(skuNum);
            skuEntity.setWareId(wareId);
            skuEntity.setStockLocked(0);
            //TODO 远程查询sku的名称，如果失败，整个事务无需回滚
            //1.自己处理异常
            //TODO 还可以用什么办法让异常出现以后不回滚呢？高级篇
            try {
                R info = productFeignService.info(skuId);
                Map<String, Object> data = (Map<String, Object>) info.get("skuInfo");
                if(info.getCode() == 0){
                    skuEntity.setSkuName((String) data.get("skuName"));
                }
            }catch (Exception e){

            }
            wareSkuDao.insert(skuEntity);
        }else {
            wareSkuDao.addStock(skuId, wareId, skuNum);
        }
    }

    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {

        List<SkuHasStockVo> collect = skuIds.stream().map(skuId -> {
            SkuHasStockVo vo = new SkuHasStockVo();

            //查询当前sku的总库存量
            //SELECT SUM(stock - stock_locked) FROM `wms_ware_sku` WHERE sku_id = 21
            Long count = baseMapper.getSkuStock(skuId);

            vo.setSkuId(skuId);
            vo.setHasStock(count ==null? false:count>0);
            return vo;
        }).collect(Collectors.toList());
        return collect;
    }

    /**
     * 为某个订单锁定库存
     * 默认只要是运行时异常都会回滚，所以rollbackFor不标也可以
     * @param vo
     *
     * 库存解锁的场景：
     * 1）下订单成功，订单成功没有支付被系统自动取消或被用户手动取消，都要库存解锁
     * 2) 下订单成功，库存锁定成功，但是接下来的业务调用失败，导致订单回滚，之前锁定的库存一段时间过后就要自动解锁
     * @return
     */
    @Transactional(rollbackFor = NoStockException.class)
    @Override
    public Boolean orderLockStock(WareSkuLockVo vo) {
        /**
         * 保存库存工作单的详情
         * 方便出问题了回溯
         */
        WareOrderTaskEntity taskEntity = new WareOrderTaskEntity();
        taskEntity.setOrderSn(vo.getOrderSn());
        wareOrderTaskService.save(taskEntity);


        //1、按照下单的收货地址，找到一个就近仓库，锁定库存
        //1、找到每个商品在哪个仓库都有库存
        List<OrderItemVo> locks = vo.getLocks();

        List<SkuWareHasStock> collect = locks.stream().map(item -> {
            SkuWareHasStock stock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            stock.setSkuId(skuId);
            stock.setNum(item.getCount());
            //查询这个商品在哪里有库存
            List<Long> wareIds  = wareSkuDao.listWareIdHasSkuStock(skuId);
            stock.setWareId(wareIds);
            return stock;
        }).collect(Collectors.toList());

        Boolean allLock = true;
        //2、锁定库存
        for (SkuWareHasStock stock : collect) {
            Boolean skuStocked = false;
            Long skuId = stock.getSkuId();
            List<Long> wareIds = stock.getWareId();
            if(wareIds == null || wareIds.size() == 0){
                //没有任何仓库有这个商品的库存
                throw new NoStockException(skuId);
            }
            //1、如果每一个商品都锁定成功，将当前商品锁定了几件的工作单记录发给MQ
            //2、锁定失败，前面保存的工作单信息就回滚了。发送出去的消息，即使要解锁消息，由于去数据库查不到Id，所以就不用解锁
            for(Long wareId : wareIds){
                //成功则返回1，否则返回0
                Long count = wareSkuDao.lockSkuStock(skuId, wareId, stock.getNum());
                if(count == 1){
                    skuStocked = true;
                    //TODO 告诉MQ库存锁定成功
                    WareOrderTaskDetailEntity detailEntity = new WareOrderTaskDetailEntity(null,
                            skuId,
                            "",
                            stock.getNum(),
                            taskEntity.getId(),
                            wareId,
                            1);
                    wareOrderTaskDetailService.save(detailEntity);
                    //
                    StockLockedTo lockedTo = new StockLockedTo();
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(detailEntity, stockDetailTo);
                    lockedTo.setId(taskEntity.getId());
                    //只发Id不行，防止回滚以后找不到数据
                    lockedTo.setDetail(stockDetailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked" , lockedTo);
                    break;
                }else {
                    //当前仓库失败，重试下一个仓库
                }
            }
            if(skuStocked == false){
                //当前商品所有仓库都没有锁住
                throw new NoStockException(skuId);
            }
        }

        return null;
    }

    @Override
    public void unlockStock(StockLockedTo to) {
            StockDetailTo detail = to.getDetail();
            Long detailId = detail.getId();
            //解锁
            //查询数据库关于这个订单的锁定库存信息
            //有： 说明库存锁定成功了，但是要不要解锁还是要看订单情况
            //如果没有这个订单，必须解锁库存；
            //没有：说明库存锁定失败了，库存回滚了，这种情况下无需解锁
            WareOrderTaskDetailEntity byId = wareOrderTaskDetailService.getById(detailId);
            if(byId != null){
                //解锁
                Long id = to.getId();
                WareOrderTaskEntity taskEntity = wareOrderTaskService.getById(id);
                String orderSn = taskEntity.getOrderSn();//根据订单号查询订单状态
                R r = orderFeignService.getOrderStatus(orderSn);
                if(r.getCode() == 0){
                    //订单数据返回成功
                    OrderVo data = r.getData(new TypeReference<OrderVo>() {
                    });
                    if(data == null || data.getStatus() == 4){
                        //订单不存在或者订单被取消了，才能解锁库存
                        if(byId.getLockStatus() == 1){
                            //只有当前库存工作单状态为1，即处于锁定状态的时候才能解锁
                            unLockStock(detail.getSkuId(), detail.getWareId(), detail.getSkuNum(), detailId);
                        }
                    }
                }else {
                    //消息拒绝以后重新放到队列里面，让别人继续消费解锁
                    throw new RuntimeException("远程服务失败");
                }
            }else {
                //无需解锁
            }
    }

    /**
     * 防止订单服务卡顿，导致订单状态一直修改不了，而又库存消息优先到达，这时候查询订单状态，订单处于新建状态，导致库存什么都不做就走了
     * 导致卡顿的服务永远不能解锁库存
     * @param orderTo
     */
    @Transactional
    @Override
    public void unlockStock(OrderTo orderTo) {
        //
        String orderSn = orderTo.getOrderSn();
        //查一下最新库存的信息，防止重复解锁库存
        WareOrderTaskEntity task = wareOrderTaskService.getOrderTaskByOrderSn(orderSn);
        Long id = task.getId();
        //按照工作单找到所有没有解锁的库存，进行解锁
        List<WareOrderTaskDetailEntity> entities = wareOrderTaskDetailService.list(new QueryWrapper<WareOrderTaskDetailEntity>().eq(
                "task_id", id
        ).eq(
                "lock_status", 1
        ));
        for (WareOrderTaskDetailEntity entity : entities) {
            //Long skuId, Long wareId, Integer num, Long taskDetailId
            unLockStock(entity.getSkuId(), entity.getWareId(), entity.getSkuNum(), entity.getId());
        }
    }

    @Data
    static class SkuWareHasStock{
        private Long skuId;
        private List<Long> wareId;
        private Integer num;
    }

}