package com.atguigu.gulimail.ware.dao;

import com.atguigu.gulimail.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-07 21:18:54
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
