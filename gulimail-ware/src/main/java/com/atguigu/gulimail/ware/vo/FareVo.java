package com.atguigu.gulimail.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author pxie2120
 * @create 2023/5/11
 */
@Data
public class FareVo {
    private MemeberAddressVo address;
    private BigDecimal fare;
}
