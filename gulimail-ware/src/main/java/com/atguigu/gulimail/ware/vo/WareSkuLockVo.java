package com.atguigu.gulimail.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/5/16
 */
@Data
public class WareSkuLockVo {
    private String orderSn;//订单号

    private List<OrderItemVo> locks;//需要锁定的所有库存信息
}
