package com.atguigu.gulimail.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/3/23
 */
@Data
public class MergeVo {
    private Long purchaseId; //整单Id
    private List<Long> items; //[1,2,3,4] 合并项集合
}
