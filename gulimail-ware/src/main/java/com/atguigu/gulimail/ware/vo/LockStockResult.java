package com.atguigu.gulimail.ware.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/5/16
 */
@Data
public class LockStockResult {
    private Long skuId;
    private Integer num;
    private Boolean locked;
}
