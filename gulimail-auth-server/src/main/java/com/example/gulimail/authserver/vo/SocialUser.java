package com.example.gulimail.authserver.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/4/20
 */
@Data
public class SocialUser {
    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private long created_at;
}
