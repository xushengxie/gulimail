package com.example.gulimail.authserver.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/4/19
 */
@Data
public class UserLoginVo {
    private String loginacct;
    private String password;
}
