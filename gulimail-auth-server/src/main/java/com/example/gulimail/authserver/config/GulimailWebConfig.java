package com.example.gulimail.authserver.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author pxie2120
 * @create 2023/4/16
 */
@Configuration
public class GulimailWebConfig implements WebMvcConfigurer {

    //视图映射
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        /**
         * @GetMapping("/login.html")
         *     public String login(){
         *
         *         return "login";
         *     }
         */
//        registry.addViewController("login.html").setViewName("login");
        /**
         * @GetMapping("/reg.html")
         *     public String reg(){
         *
         *         return "reg";
         *     }
         */
        registry.addViewController("reg.html").setViewName("reg");
    }
}
