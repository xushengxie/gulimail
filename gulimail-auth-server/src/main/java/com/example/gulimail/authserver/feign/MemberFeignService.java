package com.example.gulimail.authserver.feign;

import com.atguigu.common.utils.R;
import com.example.gulimail.authserver.vo.SocialUser;
import com.example.gulimail.authserver.vo.UserLoginVo;
import com.example.gulimail.authserver.vo.UserRegistVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author pxie2120
 * @create 2023/4/18
 */
@FeignClient("gulimail-member")
public interface MemberFeignService {
    @PostMapping("/member/member/regist")
    R regist(@RequestBody UserRegistVo vo);

    @PostMapping("/member/member/login")
    R login(@RequestBody UserLoginVo vo);

    @PostMapping("member/member/oauth/login")
    R oauthlogin(@RequestBody SocialUser socialUser) throws Exception;
}
