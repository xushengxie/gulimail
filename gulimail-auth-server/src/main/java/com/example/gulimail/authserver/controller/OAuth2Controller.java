package com.example.gulimail.authserver.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.HttpUtils;
import com.atguigu.common.utils.R;
import com.example.gulimail.authserver.feign.MemberFeignService;
import com.atguigu.common.vo.MemberResponseVo;
import com.example.gulimail.authserver.vo.SocialUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @author pxie2120
 * @create 2023/4/19
 * 处理社交登录请求
 */
@Slf4j
@Controller
public class OAuth2Controller {

    @Autowired
    MemberFeignService memberFeignService;

    /**
     * 社交登录成功回调
     * @param code
     * @return
     * @throws Exception
     */
    @GetMapping("/oauth2.0/gitee/success")
    public String gitte(@RequestParam("code") String code, HttpSession session) throws Exception {
        HashMap<String, String> header = new HashMap<>();
        HashMap<String, String> query = new HashMap<>();

        Map<String ,String> map = new HashMap<>();
        map.put("client_id", "1741ced5722cee7228693ad72ac22658a99685d74d12b94d7fcd9a07218c5f04");
        map.put("grant_type", "authorization_code");
        map.put("code", code);
        map.put("redirect_uri", "http://auth.pxie2120.com/oauth2.0/gitee/success");
        map.put("client_secret", "b012deba28b52e57b843d14e3586fc9e99a59fe244a7448fc10f78cf99abd5e5");
        //1、根据code换取AccessToken
        HttpResponse response = HttpUtils.doPost("https://gitee.com", "/oauth/token", "post", header, map, query);

        //处理
        if(response.getStatusLine().getStatusCode() == 200){
            //获取到了AccessToken
            String json = EntityUtils.toString(response.getEntity());
            SocialUser socialUser = JSON.parseObject(json, SocialUser.class);

            //知道了是哪个用户
            //1、如果当前用户是第一次进入网站，自动注册进来，并为当前用户生成一个会员信息账号，以后这个社交账号就对应用户在
            R oauthlogin = memberFeignService.oauthlogin(socialUser);
            if(oauthlogin.getCode() == 0){
                MemberResponseVo data = oauthlogin.getData("data", new TypeReference<MemberResponseVo>() {});
                log.info("登录成功：用户信息：{}",data.toString());
                //TODO 1、默认发的令牌作用域太小了
                //TODO 2、使用json序列化的方式将对象保存到redis中

                session.setAttribute("loginUser", data);
                return "redirect:http://pxie2120.com";
            }else {
                return "redirect:http://auth.pxie2120.com/login.html";
            }
        }else {
            //获取AccessToken失败，重新登录
            return "redirect:http://auth.pxie2120.com/login.html";
        }
    }
}
