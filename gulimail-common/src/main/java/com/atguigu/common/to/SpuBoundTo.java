package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author pxie2120
 * @create 2023/3/25
 */
@Data
public class SpuBoundTo {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
