package com.atguigu.common.to.mq;

import lombok.Data;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/5/23
 */
@Data
public class StockLockedTo {
    private Long id; //库存工作单的Id

    private StockDetailTo detail; //工作详情的所有Id
}
