package com.atguigu.common.exception;

/**
 * @author pxie2120
 * @create 2023/5/16
 */
public class NoStockException extends RuntimeException{
    private Long skuId;
    private String message;

    public NoStockException(Long skuId){
        super("商品id:" + skuId + "没有足够的库存了");
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getSkuId() {
        return skuId;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
