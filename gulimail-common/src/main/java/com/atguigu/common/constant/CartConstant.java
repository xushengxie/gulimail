package com.atguigu.common.constant;

/**
 * @author pxie2120
 * @create 2023/4/23
 */
public class CartConstant {
    public static final String TEMP_USER_COOKIE_NAME = "user-key";
    public static final int TEMP_USER_COOKIE_TIMEOUT = 60*60*24*30;
}
