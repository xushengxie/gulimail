package com.atguigu.common.constant;

/**
 * @author pxie2120
 * @create 2023/4/17
 */
public class AuthServerConstant {
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";
    public static final String LOGIN_USER = "loginUser";
}
