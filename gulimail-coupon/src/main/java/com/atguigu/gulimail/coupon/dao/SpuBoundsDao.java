package com.atguigu.gulimail.coupon.dao;

import com.atguigu.gulimail.coupon.entity.SpuBoundsEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品spu积分设置
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-07 21:09:02
 */
@Mapper
public interface SpuBoundsDao extends BaseMapper<SpuBoundsEntity> {
	
}
