package com.atguigu.gulimail.gateway;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;

@SpringBootTest
class GulimailGatewayApplicationTests {

    @Test
    void copy_pdf_to_another_pdf_buffer() throws FileNotFoundException {
        Long start = System.currentTimeMillis();
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream("Java并发编程：核心方法与框架.pdf"));
             BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("Java并发编程：核心方法与框架_副本.pdf"))){
            int content;
            while ((content = bis.read()) != -1){
                bos.write(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("使用缓冲流复制PDF文件总耗时: " + (end - start) + "毫秒"); //使用缓冲流复制PDF文件总耗时: 535毫秒
    }


    @Test
    void copy_pdf_to_another_pdf_stream() throws FileNotFoundException {
        Long start = System.currentTimeMillis();
        try (FileInputStream bis = new FileInputStream("Java并发编程：核心方法与框架.pdf");
             FileOutputStream bos = new FileOutputStream("Java并发编程：核心方法与框架_副本.pdf")){
            int content;
            while ((content = bis.read()) != -1){
                bos.write(content);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        long end = System.currentTimeMillis();
        System.out.println("使用普通字节流复制PDF文件总耗时: " + (end - start) + "毫秒"); //使用普通字节流复制PDF文件总耗时: 293183毫秒
    }

}
