package com.atguigu.gulimail.serarch.controller;

import com.atguigu.gulimail.serarch.service.MallSearchService;
import com.atguigu.gulimail.serarch.vo.SearchParam;
import com.atguigu.gulimail.serarch.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author pxie2120
 * @create 2023/4/4
 */
@Controller
public class SearchController {
    @Autowired
    MallSearchService mallSearchService;

    /**
     * 自动将页面提交过来的请求查询参数封装成指定的对象
     * @param param
     * @return
     */
    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model){
        //根据页面传递过来的检索参数，去ES中检索商品
        SearchResult result = mallSearchService.search(param);
        //将最终检索到的结果放到页面中进行展示
        model.addAttribute("result", result);
        return "list";
    }
}
