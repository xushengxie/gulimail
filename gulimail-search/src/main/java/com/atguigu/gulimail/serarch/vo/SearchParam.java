package com.atguigu.gulimail.serarch.vo;

import lombok.Data;

import java.util.List;

/**封装页面所有可能传递过来的查询条件
 * @author pxie2120
 * @create 2023/4/5
 */
@Data
public class SearchParam {
    //页面传过来的全文匹配关键字
    private String keyword;
    //三级分类ID
    private Long catalog3Id;

    /**
     * saleCount;skuPrice;hotScore
     */
    //排序条件
    private String sort;

    /**
     * 可以添加很多过滤条件
     * hasStock=0/1
     * skuPrice=1-500/-500/500-
     */
    private Integer hasStock; //是否只显示有货
    private String skuPrice; //价格区间
    private List<Long> brandID; //品牌ID
    private List<String> attrs; //按照属性进行筛选
    private Integer pageNum = 1; //页码

}
