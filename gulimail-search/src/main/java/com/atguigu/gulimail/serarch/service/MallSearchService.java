package com.atguigu.gulimail.serarch.service;

import com.atguigu.gulimail.serarch.vo.SearchParam;
import com.atguigu.gulimail.serarch.vo.SearchResult;

/**
 * @author pxie2120
 * @create 2023/4/5
 */
public interface MallSearchService {
    /**
     *
     * @param param 检索的所有参数
     * @return 返回检索的结果，里面页面所需要的所有信息
     */
    SearchResult search(SearchParam param);
}
