package com.atguigu.gulimail.serarch.constant;

/**
 * @author pxie2120
 * @create 2023/3/28
 */
public class EsConstant {
    public static final String PRODUCT_INDEX = "gulimail_product"; //sku数据在es中的索引
    public static final Integer PRODUCT_PAGE_SIZE = 2;
}
