package com.atguigu.gulimail.cart.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @author pxie2120
 * @create 2023/4/23
 */
@ToString
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey;
    private boolean tempUser = false;
}
