package com.atguigu.gulimail.cart.vo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author pxie2120
 * @create 2023/4/23
 * 整个购物车
 */
public class Cart {
    private List<CartItem> items;
    private Integer countNum;//商品数量
    private Integer countType; //商品类型数量
    private BigDecimal totalAmount; //商品总价
    private BigDecimal reduce  = new BigDecimal(0.00); //减免价格

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public Integer getCountNum() {
        Integer  countNum= items.stream().map(CartItem::getCount).reduce(Integer::sum).orElse(0);
        return countNum;
    }

    public Integer getCountType() {
        int countType = 0;
        if(items != null && items.size() > 0){
            for(CartItem it : items){
                countType += 1;
            }
        }
        return countType;
    }

    public BigDecimal getTotalAmount() {
        BigDecimal totalAmount = new BigDecimal("0");
        if(items != null && items.size() > 0){
            for(CartItem it : items){
                if(it.getCheck()){
                    BigDecimal totalPrice = it.getTotalPrice();
                    totalAmount = totalPrice.add(totalPrice);
                }
            }
        }

        BigDecimal subtract = totalAmount.subtract(getReduce());
        return subtract;
    }


    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
