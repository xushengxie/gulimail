package com.atguigu.gulimailtestssoclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GulimailTestSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimailTestSsoClientApplication.class, args);
    }

}
