package com.atguigu.gulimail.thirdparty.controller;

import com.atguigu.common.utils.R;
import com.atguigu.gulimail.thirdparty.component.SmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author pxie2120
 * @create 2023/4/17
 */
@RestController
@RequestMapping("/sms")
public class SmsSendController {

    @Autowired
    SmsComponent smsComponent;

    /**
     *
     * @param phone 用户手机号码
     * @return
     */
    @GetMapping("/sendcode")
    public R sendCode(@RequestParam("phone") String phone, @RequestParam("code") String code){
        smsComponent.sendMsCode(phone, code);
        return R.ok();
    }
}
