package com.atguigu.gulimail.product.fallback;

import com.atguigu.common.exception.BizCodeEnume;
import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import com.atguigu.gulimail.product.feign.SearchFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/5/26
 */
/*@Slf4j
@Component*/
public class SearchFeignServiceFallBack implements SearchFeignService {
    @Override
    public R productStatusUp(List<SkuEsModel> skuEsModels) {
//        log.info("熔断方法调用了");
        return R.error(BizCodeEnume.TO_MANY_REQUEST.getCode(), BizCodeEnume.TO_MANY_REQUEST.getMessage());
    }
}
