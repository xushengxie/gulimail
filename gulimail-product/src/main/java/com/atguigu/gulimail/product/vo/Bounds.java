/**
  * Copyright 2023 bejson.com 
  */
package com.atguigu.gulimail.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2023-03-24 21:51:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}