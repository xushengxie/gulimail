package com.atguigu.gulimail.product.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/4/13
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
