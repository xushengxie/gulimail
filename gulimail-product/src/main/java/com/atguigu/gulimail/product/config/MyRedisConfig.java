package com.atguigu.gulimail.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

/**
 * @author pxie2120
 * @create 2023/4/3
 */
@Configuration
public class MyRedisConfig {
    @Bean(destroyMethod="shutdown")
    RedissonClient redisson() throws IOException {
        //创建配置
        Config config = new Config();
        config.useSingleServer().setAddress("redis://192.168.215.138:6379");

        return Redisson.create(config);
    }
}
