/**
  * Copyright 2023 bejson.com 
  */
package com.atguigu.gulimail.product.vo;

import lombok.Data;

/**
 * Auto-generated: 2023-03-24 21:51:36
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class BaseAttrs {

    private Long attrId;
    private String attrValues;
    private int showDesc;

}