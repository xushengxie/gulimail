package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.BrandEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 品牌
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-06 22:16:33
 */
@Mapper
public interface BrandDao extends BaseMapper<BrandEntity> {
	
}
