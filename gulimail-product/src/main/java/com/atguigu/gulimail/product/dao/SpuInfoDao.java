package com.atguigu.gulimail.product.dao;

import com.atguigu.gulimail.product.entity.SpuInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * spu信息
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2023-03-06 22:16:33
 */
@Mapper
public interface SpuInfoDao extends BaseMapper<SpuInfoEntity> {
    //UPDATE `pms_spu_info` SET publish_status = ? , update_time= NOW() WHERE id = ?
    void updateSpuStatus(@Param("spuId") Long spuId, @Param("code") int code);
}
