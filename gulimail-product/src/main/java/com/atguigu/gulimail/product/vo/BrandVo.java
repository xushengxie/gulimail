package com.atguigu.gulimail.product.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/3/22
 */
@Data
public class BrandVo {
//    "brandId": 0;"brandName": "string",
    private Long brandId;
    private String brandName;
}
