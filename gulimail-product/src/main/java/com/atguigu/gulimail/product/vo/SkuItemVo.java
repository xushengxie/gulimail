package com.atguigu.gulimail.product.vo;

import com.atguigu.gulimail.product.entity.SkuImagesEntity;
import com.atguigu.gulimail.product.entity.SkuInfoEntity;
import com.atguigu.gulimail.product.entity.SpuInfoDescEntity;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @author pxie2120
 * @create 2023/4/13
 */
@Data
public class SkuItemVo {
    //1、sku的基本属性获取 pms_sku_info
    SkuInfoEntity info;
    //2、sku的图片信息 pms_sku_images
    List<SkuImagesEntity> images;
    //3、sku 的销售属性组合
    List<SkuItemSaleAttrVo> saleAttr;
    //4、spu的介绍
    SpuInfoDescEntity desc;
    //5、spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttr;



}
