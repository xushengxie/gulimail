package com.atguigu.gulimail.product.vo;

import lombok.Data;

/**
 * @author pxie2120
 * @create 2023/3/23
 */
@Data
public class AttrGroupRelationVo {
    //"attrId":1,"attrGroupId":2
    private Long attrId;
    private Long attrGroupId;
}
