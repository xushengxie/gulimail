package com.atguigu.gulimail.product.vo;

import lombok.Data;

import java.util.List;


/**
 * @author pxie2120
 * @create 2023/4/13
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    private List<AttrValueWithSkuIdVo> attrValues;
}
