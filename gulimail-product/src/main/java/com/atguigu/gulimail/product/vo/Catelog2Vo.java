package com.atguigu.gulimail.product.vo;

import com.atguigu.gulimail.product.service.CategoryService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;


/**
 * @author pxie2120
 * @create 2023/3/28
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Catelog2Vo {
    @Autowired
    CategoryService categoryService;

    private String catalog1Id; //1级父分类ID
    private List<Catelog3Vo> catalog3List; //三级子分类
    private String id;
    private String name;

    public Catelog2Vo(String catalog1Id, List<Catelog3Vo> catalog3List, String id, String name) {
        this.catalog1Id = catalog1Id;
        this.catalog3List = catalog3List;
        this.id = id;
        this.name = name;
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    public static class Catelog3Vo{
        private String catalog2Id; //2级分类ID
        private String id;
        private String name;
    }


}
